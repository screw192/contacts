import React from "react";
import {Route, Switch} from "react-router-dom";

import Layout from "./components/UI/Layout/Layout";
import Contacts from "./components/Contacts/Contacts";
import AddEditContact from "./components/AddEditContact/AddEditContact";
import './App.css';


const App = () => {
  return (
      <Layout>
        <Switch>
          <Route path="/" exact component={Contacts}/>
          <Route path="/add" exact component={AddEditContact}/>
          <Route path="/edit/:id" exact component={AddEditContact}/>
        </Switch>
      </Layout>
  );
};

export default App;