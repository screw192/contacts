import {
  FETCH_CONTACTS_FAILURE,
  FETCH_CONTACTS_REQUEST,
  FETCH_CONTACTS_SUCCESS,
  HIDE_CONTACTS_MODAL,
  SHOW_CONTACTS_MODAL,
} from "../actions/contactsActions";

const initialState = {
  contacts: {},
  loading: true,
  showModal: false,
  modalTargetContact: "",
  error: false,
  errorMsg: "",
};

const contactsReducer = (state = initialState, action) => {
  switch (action.type) {
    case SHOW_CONTACTS_MODAL:
      return {...state, showModal: true, modalTargetContact: action.id};
    case HIDE_CONTACTS_MODAL:
      return {...state, showModal: false, modalTargetContact: ""};
    case FETCH_CONTACTS_REQUEST:
      return {...state, loading: true};
    case FETCH_CONTACTS_SUCCESS:
      return {...state, loading: false, contacts: {...action.data}};
    case FETCH_CONTACTS_FAILURE:
      return {...state, loading: false, error: action.error, errorMsg: action.error.message};
    default:
      return state;
  }
};

export default contactsReducer;