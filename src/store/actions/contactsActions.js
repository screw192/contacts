import axiosOrders from "../../axios-orders";

export const SHOW_CONTACTS_MODAL = "SHOW_CONTACTS_MODAL";
export const HIDE_CONTACTS_MODAL = "HIDE_CONTACTS_MODAL";

export const FETCH_CONTACTS_REQUEST = "FETCH_CONTACTS_REQUEST";
export const FETCH_CONTACTS_SUCCESS = "FETCH_CONTACTS_SUCCESS";
export const FETCH_CONTACTS_FAILURE = "FETCH_CONTACTS_FAILURE";

export const DELETE_CONTACT_REQUEST = "DELETE_CONTACT_REQUEST";
export const DELETE_CONTACT_SUCCESS = "DELETE_CONTACT_SUCCESS";
export const DELETE_CONTACT_FAILURE = "DELETE_CONTACT_FAILURE";

export const showContactsModal = id => ({type: SHOW_CONTACTS_MODAL, id});
export const hideContactsModal = () => ({type: HIDE_CONTACTS_MODAL});

export const fetchContactsRequest = () => ({type: FETCH_CONTACTS_REQUEST});
export const fetchContactsSuccess = data => ({type: FETCH_CONTACTS_SUCCESS, data});
export const fetchContactsFailure = error => ({type: FETCH_CONTACTS_FAILURE, error});

export const fetchContacts = () => {
  return async dispatch => {
    try {
      dispatch(fetchContactsRequest());

      const response = await axiosOrders.get("/contacts.json");
      dispatch(fetchContactsSuccess(response.data));
    } catch (error) {
      dispatch(fetchContactsFailure(error));
    }
  };
};

export const deleteContactRequest = () => ({type: DELETE_CONTACT_REQUEST});
export const deleteContactSuccess = () => ({type: DELETE_CONTACT_SUCCESS});
export const deleteContactFailure = () => ({type: DELETE_CONTACT_FAILURE});

export const deleteContact = id => {
  return async dispatch => {
    try {
      dispatch(deleteContactRequest());

      await axiosOrders.delete(`/contacts/${id}.json`);
      dispatch(deleteContactSuccess());

    } catch (error) {
      dispatch(deleteContactFailure(error));

    } finally {
      dispatch(hideContactsModal());
      dispatch(fetchContacts());
    }
  };
};