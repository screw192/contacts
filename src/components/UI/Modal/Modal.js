import React from 'react';
import {Link} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";

import {deleteContact, hideContactsModal} from "../../../store/actions/contactsActions";
import Backdrop from "../Backdrop/Backdrop";
import "./Modal.css";


const Modal = () => {
  const dispatch = useDispatch();
  const targetID = useSelector(state => state.contacts.modalTargetContact);
  const targetContact = useSelector(state => state.contacts.contacts[targetID]);

  const editHandler = () => {
    dispatch(hideContactsModal());
  }

  const closeHandler = () => {
    dispatch(hideContactsModal());
  };

  const deleteHandler = () => {
    dispatch(deleteContact(targetID));
  };

  let imgBlock;
  if (targetContact.photo !== "") {
    imgBlock = (
        <img
            className="ContactPhoto"
            src={targetContact.photo}
            alt="" height="250px" width="250px"
        />
    );
  } else {
    imgBlock = <div className="NoImagePlugMed">No photo...</div>
  }

  return (
      <Backdrop>
        <div className="Modal">
          <button
              className="ModalCloseButton"
              onClick={closeHandler}
          >
            &times;
          </button>
          <div className="ContactCard">
            {imgBlock}
            <div className="ContactInfo">
              <p className="ContactName">{targetContact.name}</p>
              <p className="ContactPhone">{targetContact.phone}</p>
              <a className="ContactEmail" href={"mailto:" + targetContact.email}>{targetContact.email}</a>
            </div>
          </div>
          <div className="ContactControlBlock">
            <Link className="ContactEditButton" onClick={editHandler} to={"/edit/" + targetID}>Edit</Link>
            <button className="ContactDeleteButton" onClick={deleteHandler}>remove</button>
          </div>
        </div>
      </Backdrop>
  );
};

export default Modal;