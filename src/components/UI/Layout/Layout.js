import React from 'react';
import {useSelector} from "react-redux";

import Preloader from "../Preloader/Preloader";
import Header from "../../Header/Header";
import Modal from "../Modal/Modal";


const Layout = props => {
  const isLoading = useSelector(state => state.contacts.loading);
  const showModal = useSelector(state => state.contacts.showModal);

  let preloader;
  isLoading ? preloader = <Preloader/> : preloader = null;

  let contactsModal;
  showModal ? contactsModal = <Modal/> : contactsModal = null;

  return (
      <>
        {preloader}
        {contactsModal}
        <Header/>
        <main className="Layout-Content">
          <div className="container">
            {props.children}
          </div>
        </main>
      </>
  );
};

export default Layout;