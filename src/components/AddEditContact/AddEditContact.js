import React, {useState, useEffect} from 'react';
import {useSelector} from "react-redux";

import axiosOrders from "../../axios-orders";
import "./AddEditContact.css";


const AddEditContact = props => {
  const [contactData, setContactData] = useState({
    name: "",
    phone: "",
    email: "",
    photo: "",
  });

  const existingUserData = useSelector(state => state.contacts.contacts[props.match.params.id]);

  useEffect(() => {
    if (props.match.params.id) {
      setContactData(existingUserData);
    }
  }, [props.match.params.id, existingUserData]);

  let title;
  let saveBtnHandler;
  if (!props.match.params.id) {
    title = "Add new contact";
    saveBtnHandler = async event => {
      event.preventDefault();

      await axiosOrders.post("/contacts.json", contactData);
    };
  } else {
    title = "Edit contact";
    saveBtnHandler = async event => {
      event.preventDefault();

      await axiosOrders.put(`/contacts/${props.match.params.id}.json`, contactData);
    };
  }

  const onChangeHandler = e => {
    setContactData({
      ...contactData,
      [e.target.name]: e.target.value
    });
  };

  const backToContacts = event => {
    event.preventDefault();
    props.history.push("/");
  }

  return (
      <div className="AddEditContact">
        <h4 className="AddEditContactTitle">{title}</h4>
        <form>
          <div className="FormRow">
            <label htmlFor="inputFormName" className="FormLabel">Name</label>
            <input
                id="inputFormName"
                className="FormInput"
                type="text"
                name="name"
                value={contactData.name}
                onChange={onChangeHandler}
            />
          </div>
          <div className="FormRow">
            <label htmlFor="inputFormPhone" className="FormLabel">Phone</label>
            <input
                id="inputFormPhone"
                className="FormInput"
                type="text"
                name="phone"
                value={contactData.phone}
                onChange={onChangeHandler}
            />
          </div>
          <div className="FormRow">
            <label htmlFor="inputFormEmail" className="FormLabel">E-mail</label>
            <input
                id="inputFormEmail"
                className="FormInput"
                type="text"
                name="email"
                value={contactData.email}
                onChange={onChangeHandler}
            />
          </div>
          <div className="FormRow">
            <label htmlFor="inputFormPhoto" className="FormLabel">Photo</label>
            <input
                id="inputFormPhoto"
                className="FormInput"
                type="text"
                name="photo"
                value={contactData.photo}
                onChange={onChangeHandler}
            />
          </div>
          <div className="FormRow">
            <p className="PhotoPreviewLabel">Photo preview</p>
            <div className="ImageBox">
              <img
                  className="PhotoPreview"
                  src={contactData.photo}
                  alt=""
              />
            </div>
          </div>
          <button
              className="SaveContactButton"
              onClick={event => saveBtnHandler(event)}
          >
            Save
          </button>
          <button
              className="HomepageReturnButton"
              onClick={event => backToContacts(event)}
          >
            Back to contacts
          </button>
        </form>
      </div>
  );
};

export default AddEditContact;