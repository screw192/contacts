import React from 'react';
import {useDispatch} from "react-redux";

import {showContactsModal} from "../../../store/actions/contactsActions";
import "./Contact.css";


const Contact = ({id, image, name}) => {
  const dispatch = useDispatch();

  const onClickHandler = () => {
    dispatch(showContactsModal(id));
  };

  let imgBlock;
  if (image !== "") {
    imgBlock = <img src={image} alt="" height="100px" width="100px"/>
  } else {
    imgBlock = <div className="NoImagePlug">No photo...</div>
  }

  return (
      <div
          className="Contact"
          onClick={onClickHandler}
      >
        {imgBlock}
        <p>{name}</p>
      </div>
  );
};

export default Contact;