import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";

import {fetchContacts} from "../../store/actions/contactsActions";
import Contact from "./Contact/Contact";
import "./Contacts.css";


const Contacts = () => {
  const dispatch = useDispatch();
  const contacts = useSelector(state => state.contacts.contacts);

  useEffect(() => {
    dispatch(fetchContacts());
  }, [dispatch]);

  const contactsList = Object.keys(contacts).map(contactKey => {
    return (
        <Contact
            key={contactKey}
            id={contactKey}
            name={contacts[contactKey].name}
            image={contacts[contactKey].photo}
        />
    );
  });

  return (
      <div className="ContactsBlock">
        {contactsList}
      </div>
  );
};

export default Contacts;