import React from 'react';
import {Link} from "react-router-dom";

import "./Header.css";


const Header = () => {
  return (
      <header className="Header">
        <div className="container">
          <div className="HeaderInner">
            <h2 className="HeaderTitle">Contacts</h2>
            <Link
                to="/add"
                className="AddContactButton"
            >
              Add new contact
            </Link>
          </div>
        </div>
      </header>
  );
};

export default Header;